/* SimpleApp.scala */
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import com.datastax.spark.connector._

object SimpleApp {
  def main(args: Array[String]) {
    val conf = new SparkConf(true)
      .set("spark.cassandra.connection.host", "127.0.0.1")

    val sc = new SparkContext("spark://127.0.0.1:7077", "simple", conf)
    
    val rdd = sc.cassandraTable("userdb", "mycount")
    
    val cnt:Long = rdd.count
    
    val row:CassandraRow = rdd.first
    
    val out = s"Row count is: %d\nThe first of which is:\n%s".format(cnt, row.toString)
    
    println(out)
  }
}